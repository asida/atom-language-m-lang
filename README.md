# MLang language package for atom 1.58+

MUMPS (m-lang) language support for atom, makes nice syntax highlight

see original at [github](https://github.com/ksherlock/MUMPS.tmbundle)

<img src="https://gitlab.com/asida/atom-language-m-lang/-/raw/main/screenshot.png" width="600" alt="screenshot"/>

## how to install
I am not experienced enough to register it as regular atom plugin (sorry), so to make it work just follow steps below:

```
#to make it work just
cd ~/.atom/packages
git clone https://gitlab.com/asida/atom-language-m-lang
#consider some chmods
```

## status
stable, may be missing couple intristic functions but overal superb.

## Usage

just name your file with .m extension and it gets colored as mumps source code

## source package info

```
#converted using:
apm init --package atom-m-lang --convert https://github.com/ksherlock/MUMPS.tmbundle
```

thanks to ksherlock
